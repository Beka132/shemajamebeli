package com.example.shemajamebeli

fun maxMin(array1:ArrayList<Int>):String{

    var min=0
    var max=0
    min= array1.min()!!
    array1.drop(min)
    min=array1.min()!!


    max= array1.max()!!
    array1.drop(max)
    max= array1.max()!!

    return "second min $min second max $max"
}

fun main() {

    var array1: ArrayList<Int> = arrayListOf(1,2,3,4)

    println(maxMin(array1))

}